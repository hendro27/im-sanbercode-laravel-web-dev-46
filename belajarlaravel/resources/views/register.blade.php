<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign up form</h2>
    <form action="welcome.html" method="post">
        <label>First Name:</label> <br> <br>
        <input type="text"> <br> <br>
        <label>Last Name:</label> <br> <br>
        <input type="text"> <br> <br> 
        <label>Gender:</label> <br> <br>
        <input type="radio"name="status">Male <br>
        <input type="radio"name="status">Female <br>
        <input type="radio"name="status">Other <br> <br>
        <label >Nationality:</label> <br> <br>
        <select name="Nationality">
            <option value="">Indonesia</option>
            <option value="">Inggris</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox" name="Language Spoken"> Bahasa indonesia <br>
        <input type="checkbox" name="Language Spoken"> English <br>
        <input type="checkbox" name="Language Spoken"> Other <br> <br>
        <label> Bio:</label> <br> <br>
        <textarea cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign up">
    </form>
</body>
</html>