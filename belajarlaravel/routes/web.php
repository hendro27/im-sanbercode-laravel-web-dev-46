<?php

use Illuminate\Support\Facades\Route;
use app\http\controllers\homecontroller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//entpoint/route

//testing master template
Route::get('/master', function () {
    return view('layouts.master');
});
