<?php
require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$hewan = new animal("shaun");
echo "name : " . $hewan->name . "<br>";
echo "legs : " . $hewan->legs . "<br>";
echo "cold blooded : " . $hewan->cold_blooded . "<br>";

echo "<br>";

$kodok = new animal("buduk");
echo "name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
echo "hop hop : " . $kodok->jump() . "<br>";

echo "<br>";

$sungokong = new animal("buduk");
echo "name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
echo "yell  : " . $sungokong->yell() . "<br>";